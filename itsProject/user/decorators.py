from django.shortcuts import redirect
from itsProject.settings import startUrl

def check_user_login(view,):
    def wrap(request, *args, **kwargs):
        if 'userId' not in request.session:
            return redirect( startUrl + "register/")
        else:
            return view(request, **kwargs)

    return wrap