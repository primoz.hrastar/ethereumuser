from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'user'

urlpatterns = [
    path('', views.user, name='user'),

    # register
    path('register/', views.registerView, name='register'),
    path('registerAction/', views.registerAction, name='registerAction'),

    #login logout
    path('login/', views.loginView, name='login'),
    path('loginAction/', views.loginAction, name='loginAction'),
    path('logout/', views.logout, name='logout'),

    #wallet
    path('wallet/', views.walletView, name='walletView'),
    path('wallet/pk', views.walletPKView, name='walletPKView'),
    path('wallet/changePassword', views.changeWalletPassword, name='changeWalletPassword'),

    ]
