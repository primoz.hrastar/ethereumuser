import hashlib
from django.shortcuts import get_object_or_404
from user.models import User, Wallet

#creates user model, wallet
#returns status 1-OK, 0-ERR
def registerFunction(email, password):

    if User.objects.filter(email=email).count() != 0:
        return 0

    passwordHash = createPasswordHash(password)

    #create user
    user = User()
    user.email = email
    user.password = passwordHash
    user.save()

    #create users ETH wallet
    wallet = Wallet()
    wallet.newWallet(password)
    wallet.user = user
    wallet.save()

    return 1


#returns user.id on succeess
def loginFunction(email, password):
    try:
        user = get_object_or_404(User, email=email)
    except:
        return 0

    if isPasswordCorrect(password, user):
        return user.id

    return 0

def createPasswordHash(password):
    passwordHash = hashlib.md5(str(password).encode('utf-8')).hexdigest()
    return passwordHash

def isPasswordCorrect(password, user):
    passwordHash = hashlib.md5(str(password).encode('utf-8')).hexdigest()
    if passwordHash == user.password:
        return True
    return False

