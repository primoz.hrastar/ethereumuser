from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from user.decorators import check_user_login
from user.userLogic import registerFunction, loginFunction
from user.models import User, Wallet
from itsProject.settings import startUrl


# Create your views here.

@check_user_login
def user(request):

    userId = request.session['userId']
    user = get_object_or_404(User, id=userId)
    wallet = get_object_or_404(Wallet, user=user)
    context = {
        'user': user,
        'wallet': wallet,
    }

    return render(request, "user/userProfile.html", context)


#register template
def registerView(request):
    return render(request, "user/register.html", {})


#gets POST data, and calls registerFunction
#on bad call redirects back
#success: to login
def registerAction(request):

    email = request.POST['email']
    password = request.POST['password']

    status = registerFunction(email, password)

    if status == 1:
        return redirect( startUrl + "login/")

    return redirect(startUrl + "register/")


#login template
def loginView(request):
    return render(request, "user/login.html", {})


#gets POST data, and calls loginFunction
#on bad call redirects back
#success: to homepage
def loginAction(request):
    if 'userId' in request.session:
        return redirect(startUrl)

    email = request.POST['email']
    password = request.POST['password']

    status = loginFunction(email, password)
    if status > 0:
        request.session['userId'] = str(status)
        return redirect(startUrl)

    return redirect(startUrl + "login/")

#delete session
def logout(request):
    del request.session['userId']
    return redirect(startUrl)


#wallet page view
def walletView(request):

    userId = request.session['userId']
    user = get_object_or_404(User, id=userId)
    wallet = get_object_or_404(Wallet, user=user)

    balance = wallet.getBalance()

    context = {
        'user': user,
        'wallet': wallet,
        'balance': balance,
    }

    return render(request, "wallet/wallet.html", context)


#PRIVATE KEY page, wallet/pk.html
def walletPKView(request):

    password = request.POST['PKpassword']
    userId = request.session['userId']
    user = get_object_or_404(User, id=userId)
    wallet = get_object_or_404(Wallet, user=user)

    pk = wallet.unlockWallet(password)

    context = {
        'pk': pk,
    }

    return render(request, "wallet/pk.html", context)


#tries to change wallet password, not account pass
def changeWalletPassword(request):

    oldPassword = request.POST['oldPassword']
    newPassword = request.POST['newPassword']

    userId = request.session['userId']
    user = get_object_or_404(User, id=userId)
    wallet = get_object_or_404(Wallet, user=user)

    try:
        wallet.changeWalletPassword(oldPassword,newPassword)
    except:
        return HttpResponse("error")

    return redirect(startUrl + "wallet/")
