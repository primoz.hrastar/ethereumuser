import json

from django.db import models
from web3 import Web3, HTTPProvider, Account
from itsProject.settings import URL_INFURA as urlInfura


# Create your models here.
class User(models.Model):
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    status = models.IntegerField(default=0)
    def __str__(self):
        return self.email


class Wallet(models.Model):
    address = models.CharField(max_length=255, default="")
    keystore = models.TextField(default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    locked = models.BooleanField(default=False)

    def __str__(self):
        return self.address

    #creates wallet, sets address and keystore
    def newWallet(self, password):
        if(self.address == "" and self.keystore == ""):
            web3 = Web3(HTTPProvider(urlInfura))
            acc = web3.eth.account.create()
            ethAddress = acc.address
            keystore = Account.encrypt(acc.privateKey, password)
            #ethKey = web3.toHex(acc.privateKey)
            self.address = ethAddress
            self.keystore = str(keystore)

    #gets private key from keystore and password
    def unlockWallet(self, password):
        web3 = Web3(HTTPProvider(urlInfura))
        keystoreDict = eval(self.keystore)
        try:
            privateKey = Account.decrypt(keystoreDict, password)
        except:
            return ""
        return web3.toHex(privateKey)

    #generates new keystore with new password and pk
    def changeWalletPassword(self, oldPassword, newPassword):
        web3 = Web3(HTTPProvider(urlInfura))
        keystoreDict = eval(self.keystore)
        privateKey = Account.decrypt(keystoreDict, oldPassword)
        keystore = Account.encrypt(privateKey, newPassword)
        self.keystore = str(keystore)
        self.save()

    #returns current eth balance of address
    def getBalance(self,):
        web3 = Web3(HTTPProvider(urlInfura))
        balance = web3.eth.getBalance(str(self.address))
        return balance

